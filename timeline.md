# GSoC 2017 Full Program Timeline

|Event|Date|Information|
|-----|----|-----------|
|Organization Applications Open|January 19, 2017|Open source organizations that would like to participate as a mentor organization in this year program can apply.|
|Organization Application Deadline|February 9, 2017|All organizations wishing to be a part of GSoC 2017 must submit their application by 16:00 UTC.|
|Organization Application review|Feburary 10-26, 2017|Google program administrators review organization applications|
|Organizations Announced|February 27, 2017|List of accepted mentoring organizations published.|
|Organizations-Student discussion|Feburary 27 - March 20, 2017|Potential student participants discuss application ideas with mentoring organizations|
|Student Application Period|March 20 - April 3, 2017|Students can register and submit their applications to mentor organizations. All proposals must be submitted by 16:00 UTC on April 3rd.|
|Student Projects Announced|May 4, 2017|Accepted students are paired with a mentor and start planning their projects and milestones.|
|Community Bonding|May 5 - May 29, 2017|Students spend three and half week learning more about their organizations community.|
|Students Work on their Projects|May 30 - June 26, 2017|Students begin coding for their Google Summer of Code 2017 projects. This is the first half of the coding period.|
|Phase 1 Evaluations|June 26 - 30, 2017|Mentors and students submit their evaluations of one another. These evaluations are a required step of the program. Evaluations must be subimted by 16:00 UTC on June 30|
|Students Continue Coding|July 1 - 23 , 2017|Students continue working on their project with guidance from Mentors|
|Phase 2 Evaluations|July 24 - 28, 2017|Mentors and students submit their evaluations of one another. These evaluations are a required step of the program. Evaluations must be subimted by 16:00 UTC on July 28|
|Students Continue Coding|July 29 - August 20 , 2017|Students continue working on their project with guidance from Mentors|
|Students Submit Code and Evaluations|August 21 - 29, 2017|Final week: Students clean their code, write tests, improve documentation, and submit their code samples. Students also complete their final evaluations of mentors.|
|Mentors Submit Final Evaluations|August 29 - September 5, 2017|Mentors review student code samples and determine if the students have successfully completed their Google Summer of Code 2017 project.|
|Results Announced|Septemner, 2017|Students are notified of the pass/fail status of their Google Summer of Code 2017 projects.|
|Mentor Summit|Late October 2017|Mentor Summit at the Google Campus|
